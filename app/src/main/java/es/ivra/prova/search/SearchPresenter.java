package es.ivra.prova.search;

import android.support.annotation.NonNull;

import java.util.List;

import es.ivra.prova.base.BasePresenter;
import es.ivra.prova.model.ImageModel;

public class SearchPresenter extends BasePresenter implements SearchInteractor.onSearchFetched {

    private static final String TAG = SearchPresenter.class.getSimpleName();

    private SearchView view;
    private SearchInteractor searchInteractor;


    SearchPresenter(SearchView view, @NonNull SearchInteractor searchInteractor) {
        this.view = view;
        this.searchInteractor = searchInteractor;
    }

    void fetchData(String searchString){
        view.showLoading();
        searchInteractor.remoteFetch(this, searchString);
    }


    @Override
    public void onSuccess(List<ImageModel> images) {
        view.hideLoading();
        view.showResultSearch(images);
    }

    @Override
    public void onFailure() {
        view.hideLoading();
    }
}
