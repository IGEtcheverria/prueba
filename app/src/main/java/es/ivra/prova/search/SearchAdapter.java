package es.ivra.prova.search;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import es.ivra.prova.R;
import es.ivra.prova.model.ImageModel;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.RecyclerViewAdapter> {

    private Context context;
    private List<ImageModel> images;
    private ItemClickListener itemClickListener;

    public SearchAdapter(Context context, List<ImageModel> images, ItemClickListener itemClickListener) {
        this.context = context;
        this.images = images;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerViewAdapter holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_search, viewGroup, false);

        return new RecyclerViewAdapter(view, itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter recyclerViewAdapter, int position) {
        ImageModel imageModel = images.get(position);

        recyclerViewAdapter.setAdapter(
                imageModel.getAuthor(),
                imageModel.getTitle(),
                imageModel.getUrl()
        );
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_title, tv_author;
        ImageView iv_image;
        ItemClickListener itemClickListener;
        LinearLayout ll_image;

        public RecyclerViewAdapter(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_author = itemView.findViewById(R.id.tv_author);
            iv_image = itemView.findViewById(R.id.iv_image);
            ll_image = itemView.findViewById(R.id.ll_image);

            ll_image.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(v, getAdapterPosition());
        }

        public void setAdapter(String author, String title, String imageUrl) {
            tv_author.setText(author);
            tv_title.setText(title);


            Glide.with(context)
                    .load(imageUrl)
                    .thumbnail(0.5f)
                    .disallowHardwareConfig()
                    .into(iv_image);


        }
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void clear() {
        images.clear();
    }
}
