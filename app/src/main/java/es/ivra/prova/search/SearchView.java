package es.ivra.prova.search;

import java.util.List;

import es.ivra.prova.model.ImageModel;

public interface SearchView {
    void showLoading();
    void hideLoading();
    void fetchResultSearch();
    void showResultSearch(List<ImageModel> images);
    void onErrorLoading(String message);
}
