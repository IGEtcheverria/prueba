package es.ivra.prova.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.ivra.prova.R;
import es.ivra.prova.base.BaseActivity;
import es.ivra.prova.detail.DetailActivity;
import es.ivra.prova.model.ImageModel;

public class SearchActivity extends BaseActivity<SearchPresenter> implements SearchView{

    @BindView(R.id.rv_search)
    RecyclerView recyclerView;
    @BindView(R.id.et_search)
    EditText et_search;
    @BindView(R.id.iv_search)
    ImageView iv_search;
    @BindView(R.id.pb_search)
    ProgressBar pb_search;

    private SearchAdapter adapter;
    private SearchAdapter.ItemClickListener itemClickListener;
    private List<ImageModel> imagesFlickr = new ArrayList<>();

    @NonNull
    @Override
    protected SearchPresenter createPresenter(@NonNull Context context) {
        return new SearchPresenter(this, new SearchInteractor());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ButterKnife.bind(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        itemClickListener = ((view, position) -> {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra("image", imagesFlickr.get(position));
            startActivity(intent);
        });

    }

    @Override
    public void showLoading() {
        pb_search.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pb_search.setVisibility(View.GONE);
    }

    @Override
    public void fetchResultSearch() {
        mPresenter.fetchData(et_search.getText().toString());
    }

    @Override
    public void showResultSearch(List<ImageModel> images) {
        adapter = new SearchAdapter(this, images, itemClickListener);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        imagesFlickr = images;
    }

    @Override
    public void onErrorLoading(String message) {
    }

    @OnClick(R.id.iv_search)
    public void onViewClick(){
        fetchResultSearch();
    }

}
