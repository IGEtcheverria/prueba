package es.ivra.prova.search;

import java.util.List;

import es.ivra.prova.api.ApiFlickr;
import es.ivra.prova.api.ApiInterface;
import es.ivra.prova.model.ImageModel;

public class SearchInteractor implements ApiInterface {
    private static final String TAG = SearchInteractor.class.getSimpleName();
    private onSearchFetched listener;

    public interface onSearchFetched {
        void onSuccess(List<ImageModel> images);
        void onFailure();
    }


    public void remoteFetch(final onSearchFetched listener, String search) {
        this.listener = listener;

        ApiFlickr myAsyncTask = new ApiFlickr(this, search);
        myAsyncTask.execute();
    }


    @Override
    public void onTaskCompleted(List<ImageModel> images) {
        listener.onSuccess(images);
    }

    @Override
    public void onTaskError() {
        listener.onFailure();
    }

}
