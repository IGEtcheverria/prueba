package es.ivra.prova.api;

import android.os.AsyncTask;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.SearchParameters;

import java.util.ArrayList;
import java.util.List;

import es.ivra.prova.model.ImageModel;

public class ApiFlickr extends AsyncTask<String, Void, String> {

    private ApiInterface listener;
    private String search;
    private Boolean isDetail;
    List<ImageModel> images = new ArrayList<>();

    public ApiFlickr(ApiInterface listener, String search){
        this.listener = listener;
        this.search = search;
        this.isDetail = isDetail;
    }

    @Override
    protected String doInBackground(String... strings) {

        String apiKey = "16412ce7aefd4f0c25278f7e3dc4ab91";
        String sharedSecret = "9a9e55aa9b4c522b";
        REST rest = new REST();
        Flickr flickrClient = new Flickr(apiKey, sharedSecret, rest);

        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setLatitude("41.388362");
        searchParameters.setLongitude("2.148401");
        searchParameters.setText(search);
        searchParameters.setRadius(3); // Km around the given location where to search pictures

        PhotoList<Photo> photos = null;
        try {
            photos = flickrClient.getPhotosInterface().search(searchParameters, 5, 1);

        } catch (FlickrException e) {
            e.printStackTrace();
            listener.onTaskError();
        }

        assert photos != null;
        for (Photo p : photos) {
            ImageModel image = new ImageModel(
                    p.getId(),
                    p.getTitle(),
                    p.getOwner().getUsername(),
                    p.getUrl(),
                    p.getFarm(),
                    p.getServer(),
                    p.getSecret()
            );

            images.add(image);
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s){
        super.onPostExecute(s);
        listener.onTaskCompleted(images);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
}
