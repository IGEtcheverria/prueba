package es.ivra.prova.api;

import java.util.List;

import es.ivra.prova.model.ImageModel;

public interface ApiInterface {
    void onTaskCompleted(List<ImageModel> images);
    void onTaskError();
}
