package es.ivra.prova.model;

import java.io.Serializable;

public class ImageModel implements Serializable {

    private String id;
    private String title;
    private String author;
    private String date;
    private String description;
    private String image;
    private String farm;
    private String server;
    private String secret;

    public ImageModel(){

    }

    public ImageModel(String id, String title, String author, String image, String farm, String server, String secret){
        this.id = id;
        this.title = title;
        this.author = author;
        this.image = image;
        this.farm = farm;
        this.server = server;
        this.secret = secret;
    }

    private ImageModel(String id, String title, String author, String date, String description){
        this.id = id;
        this.title = title;
        this.author = author;
        this.date = date;
        this.description = description;
    }

    public String getUrl() {
        return "http://farm" + farm + ".static.flickr.com/" + server + "/" + id + "_" + secret + ".jpg";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFarm() {
        return farm;
    }

    public void setFarm(String farm) {
        this.farm = farm;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
