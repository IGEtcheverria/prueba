package es.ivra.prova.detail;

import android.os.Bundle;

import es.ivra.prova.model.ImageModel;
import es.ivra.prova.search.SearchInteractor;

public class DetailInteractor {

    private static final String TAG = SearchInteractor.class.getSimpleName();

    public interface onSearchFetched{
        void onSuccess(ImageModel image);
        void onFailure();
    }

    public void getImageFlickr(final DetailInteractor.onSearchFetched listener, Bundle bundle){
        if(bundle != null){
            listener.onSuccess((ImageModel) bundle.getSerializable("image"));
        }else {
            listener.onFailure();
        }
    }


}
