package es.ivra.prova.detail;

import android.os.Bundle;

import es.ivra.prova.base.BasePresenter;
import es.ivra.prova.model.ImageModel;

public class DetailPresenter extends BasePresenter implements DetailInteractor.onSearchFetched{

    private static final String TAG = DetailPresenter.class.getSimpleName();

    private DetailView view;
    private DetailInteractor detailInteractor;

    DetailPresenter(DetailView view, DetailInteractor detailInteractor) {
        this.view = view;
        this.detailInteractor = detailInteractor;
    }

    void fetchData(Bundle bundle){
        view.showLoading();
        detailInteractor.getImageFlickr(this, bundle);
    }

    @Override
    public void onSuccess(ImageModel image) {
        view.hideLoading();
        view.showResultDetail(image);
    }

    @Override
    public void onFailure() {
        view.onErrorLoading("");
    }
}
