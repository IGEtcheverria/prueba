package es.ivra.prova.detail;

import android.os.Bundle;

import es.ivra.prova.model.ImageModel;

public interface DetailView {
    void showLoading();
    void hideLoading();
    void fetchResultDetail(Bundle bundle);
    void showResultDetail(ImageModel image);
    void onErrorLoading(String message);

}
