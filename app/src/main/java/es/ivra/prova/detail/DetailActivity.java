package es.ivra.prova.detail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.ivra.prova.R;
import es.ivra.prova.base.BaseActivity;
import es.ivra.prova.model.ImageModel;

public class DetailActivity extends BaseActivity<DetailPresenter> implements DetailView {

    @BindView(R.id.pb_loading)
    ProgressBar pb_loading;
    @BindView(R.id.iv_image)
    ImageView iv_image;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_description)
    TextView tv_description;

    @NonNull
    @Override
    protected DetailPresenter createPresenter(@NonNull Context context) {
        return new DetailPresenter(this, new DetailInteractor());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        fetchResultDetail(bundle);

    }


    @Override
    public void showLoading() {
        pb_loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pb_loading.setVisibility(View.GONE);
    }

    public void fetchResultDetail(Bundle bundle) {
        mPresenter.fetchData(bundle);
    }

    @Override
    public void showResultDetail(ImageModel image) {
        tv_description.setText(image.getDescription());
        tv_title.setText(image.getTitle());
        Glide.with(this)
                .load(image.getUrl())
                .apply(RequestOptions.centerCropTransform())
                .into(iv_image);
    }

    @Override
    public void onErrorLoading(String message) {

    }
}
